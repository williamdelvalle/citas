<?php

namespace Drupal\Tests\citas\Functional;

use Drupal\citas\CitasStorage;
use Drupal\Tests\BrowserTestBase;

/**
 * Pruebas funcionales
 *
 * @group phpunit_citas
 */
class CitasTest extends BrowserTestBase {

  /**
   * Módulos para habilitar
   *
   * @var array
   */
  public static $modules = ['citas', 'block'];

  /**
   * Perfil de instalación.
   *
   * Mínimo necesitamos el perfil 'minimal' para asegurarnos que el bloque Herramientas esté disponible
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    // Add the system menu blocks to appropriate regions.
    $this->setupMenus();
  }

  /**
   * Set up menus and tasks in their regions.
   *
   * Since menus and tasks are now blocks, we're required to explicitly set them
   * to regions. This method standardizes the way we do that for Examples.
   *
   * Note that subclasses must explicitly declare that the block module is a
   * dependency.
   */
  protected function setupMenus() {
    $this->drupalPlaceBlock('system_menu_block:tools', ['region' => 'primary_menu']);
    $this->drupalPlaceBlock('local_tasks_block', ['region' => 'secondary_menu']);
    $this->drupalPlaceBlock('local_actions_block', ['region' => 'content']);
    $this->drupalPlaceBlock('page_title_block', ['region' => 'content']);
  }

  /**
   * Prueba inicial.
   *
   * Verificamos lo siguiente:
   * - Verificamos que se han insertado dos registros en la tabla.
   * - Probamos la página inicial del módulo.
   * - Verificamos que el módulo tiene su enlace en el menú herramientas.
   */
  public function testCitas() {
    $assert = $this->assertSession();

    // Verificamos que se han insertado dos registros en la tabla.
    $result = CitasStorage::load();
    $this->assertCount(2, $result, 'No se encontraron dos registros en la tabla después de instalar el módulo.');

    // Probamos la página inicial del módulo.
    $this->drupalGet('/citas');
    $assert->statusCodeEquals(200);

    // Verificamos que el módulo tiene su enlace en el menú herramientas.
    $links = $this->providerMenuLinks();
    foreach ($links as $page => $hrefs) {
      foreach ($hrefs as $href) {
        $this->drupalGet($page);
        $assert->linkByHrefExists($href);
      }
    }
  }

  /**
   * Probamos los enlaces del menú.
   *
   * @return array
   *   Arreglo de Enlaces a comprobar:
   *   - La clave es la ruta a la página donde debe aparecer nuestro enlace.
   *   - El valor es un arreglo de enlaces que deben aparecer en esa página.
   */
  protected function providerMenuLinks() {
    return array(
      '' => array(
        '/citas',
      ),
      '/citas' => array(
        '/citas/add',
        '/citas/update',
      ),
    );
  }

  /**
   * Probamos la interfaz
   */
  public function testUI() {
    $assert = $this->assertSession();

    $this->drupalLogin($this->createUser());
    // Probamos la lista.
    $this->drupalGet('/citas');
    $assert->statusCodeEquals(200);
    $assert->pageTextMatches('%William Del Valle[td/<>\w\s]+williamdelvalle@gmail.com%');

    // Agregamos una cita
    $this->drupalPostForm(
      '/citas/add',
      array(
        'nombre' => 'Santiago Del Valle',
        'documento' => '12345678',
        'email' => 'santiago@email',
        'fecha' => date('Y-m-d', strtotime('+2 days')),
        'hora' => date('H:i', time()),
        'descripcion' => 'Cita de Santiago',
      ),
      'Add'
    );
    // Buscamos la nueva cita.
    $this->drupalGet('/citas');
    $assert->pageTextMatches('%Santiago Del Valle[td/<>\w\s]+santiago@email%');

    // Buscamos una cita por medio del documento
    $result = CitasStorage::load(array('documento' => '12345678'));
    $this->drupalGet('/citas');
    $this->assertCount(1, $result, 'No se encontró una cita con el documento = "12345678".');
    $entry = $result[0];
    unset($entry->id);

    $entry = ['nombre' => 'Andres Ruiz', 'email' => 'andres@email', 'descripcion' => 'Cita de Andres'];
    $this->drupalPostForm('/citas/update', $entry, 'Update');

    // Buscamos la cita modificada
    $this->drupalGet('/citas');
    $assert->pageTextMatches('%Andres Ruiz[td/<>\w\s]+andres@email%');

    // Intentamos crear una cita sin estar logueados
    $this->drupalLogout();
    $this->drupalPostForm(
      '/citas/add',
      array(
        'nombre' => 'Claudia Polo',
        'documento' => '74653872',
        'email' => 'claudia@email',
        'fecha' => date('Y-m-d', strtotime('+3 days')),
        'hora' => date('H:i', time()),
        'descripcion' => 'Cita de Claudia',
      ),
      'Add'
    );
    $assert->pageTextContains('Debe iniciar sesión para agregar citas.');
  }

  /**
   * Probamos el almacenamiento, agregando, actualizando y borrando citas
   */
  public function testCitasStorage() {

    // Creamos una cita
    $entry = array(
      'nombre' => 'Edwin Ruiz',
      'documento' => '63725463',
      'email' => 'repeat@email',
      'fecha' => strtotime('+4 days'),
      'hora' => time(),
      'descripcion' => 'Cita de Edwin',
    );
    CitasStorage::insert($entry);

    // Creamos otra cita
    $entry = array(
      'nombre' => 'Ana Del Valle',
      'documento' => '27384657',
      'email' => 'repeat@email',
      'fecha' => strtotime('+5 days'),
      'hora' => time(),
      'descripcion' => 'Cita de Ana',
    );
    CitasStorage::insert($entry);

    // Verificamos que existan 4 citas en la tabla
    $result = CitasStorage::load();
    $this->assertCount(4, $result);

    // Verificamos que dos de esas citas tengan el email 'repeat@email'
    $result = CitasStorage::load(array('email' => 'repeat@email'));
    $this->assertCount(2, $result, 'No se encontraron dos registros en la tabla con email = "repeat@email".');

    // Buscamos la cita con email = 'wendy@email'
    $result = CitasStorage::load(array('email' => 'wendy@email'));

    // Verificamos que encontramos una cita con email 'wendy@email'.
    $this->assertCount(1, $result, 'No se encontró un registro en la tabla con email = "wendy@email"');

    // El email 'wendy@email' lo cambiamos a 'wendy@local'.
    $entry = $result[0];
    $entry->email = "wendy@local";

    // El método update() devuelve el número de registros modificados
    $this->assertNotEquals(CitasStorage::update((array) $entry), 0);

    $result = CitasStorage::load(array('email' => 'wendy@local'));
    $this->assertCount(1, $result, "No se encontró el renombrado email 'wendy@local'.");

    // Buscamos la cita con nombre = 'William Del Valle' y email = 'williamdelvalle@gmail.com'
    $result = CitasStorage::load(array('nombre' => 'William Del Valle', 'email' => 'williamdelvalle@gmail.com'));
    $this->assertCount(1, $result, 'No se encontró el registro de William Del Valle.');

    // Obtenemos la cita
    $entry = (array) end($result);
    // Cambiamos el email a 'william@email'
    $entry['email'] = 'william@email';
    // Actualizamos la cita en la tabla
    CitasStorage::update((array) $entry);

    // Buscamos la cita con email = 'william@email'
    // Read only William Del Valle entry.
    $result = CitasStorage::load(array('email' => 'william@email'));
    // Verificamos que encontramos una cita con email = 'william@email'
    $this->assertCount(1, $result, 'No se encontró un registro en la tabla con email = william@email.');

    // Verificamos que sea la cita con nombre = 'William Del Valle'
    $entry = (array) end($result);
    // El nombre 'William Del Valle' se encuentra en el registro
    $this->assertEquals('William Del Valle', $entry['nombre'], 'El nombre William Del Valle no fue encontrado en el registro.');
    // El email 'william@email' se encuentra en el registro
    $this->assertEquals('william@email', $entry['email'], 'El email william@email no fue encontrado en el registro.');

    // Borramos el registro
    CitasStorage::delete($entry);

    $result = CitasStorage::load();
    // Verificamos que ahora solo hay 3 registros
    $this->assertCount(3, $result, 'No se encontraron solo tres registros, un registro no ha sido borrado.');
  }

}
