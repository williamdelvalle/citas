<?php

/**
 * @file
 * CitasUpdateForm
 */

namespace Drupal\citas\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\citas\CitasStorage;
use Drupal\Core\Url;

/**
 * Sample UI to update a record.
 */
class CitasUpdateForm extends FormBase {

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'citas_update_form';
  }

  /**
   * Sample UI to update a record.
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL) {
    // Wrap the form in a div.
    $form = [
      '#prefix' => '<div id="updateform">',
      '#suffix' => '</div>',
    ];

    // Add some explanatory text to the form.
    $form['message'] = [
      '#markup' => t('Update an appointment.'),
    ];

    // Query for items to display.
    $entries = CitasStorage::load();
    // Tell the user if there is nothing to display.
    if (empty($entries)) {
      $form['no_values'] = [
        '#value' => t('There are no appointments.'),
      ];

      return $form;
    }

    $keyed_entries = [];
    foreach ($entries as $entry) {
      $options[$entry->id] = t('@id: @nombre (@fecha @hora)', [
        '@id' => $entry->id,
        '@nombre' => $entry->nombre,
        '@fecha' => date('Y-m-d', $entry->fecha),
        '@hora' => date('H:i', $entry->hora),
      ]);
      $keyed_entries[$entry->id] = $entry;
    }

    // Grab the id.
    if ($id == NULL) {
      $id = $form_state->getValue('id');
    } else {
      if(!is_object($keyed_entries[$id])) {
        $id = 0;
      }
    }

    // Use the id to set the default entry for updating.
    $default_entry = !empty($id) ? $keyed_entries[$id] : $entries[0];

    // Save the entries into the $form_state. We do this so the AJAX callback
    // doesn't need to repeat the query.
    $form_state->setValue('entries', $keyed_entries);

    $form['id'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => t('Choose appointment to update'),
      '#default_value' => $default_entry->id,
      '#ajax' => [
        'wrapper' => 'updateform',
        'callback' => [$this, 'updateCallback'],
      ],
    ];
    $form['nombre'] = [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#size' => 30,
      '#default_value' => $default_entry->nombre,
      '#required' => TRUE,
    ];
    $form['documento'] = [
      '#type' => 'textfield',
      '#title' => t('Document'),
      '#size' => 15,
      '#value' => $default_entry->documento,
      '#attributes' => ['readonly' => 'readonly'],
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => t('Email'),
      '#size' => 30,
      '#default_value' => $default_entry->email,
      '#required' => TRUE,
    ];
    $form['fecha'] = [
      '#type' => 'date',
      '#title' => t('Date'),
      '#default_value' => date('Y-m-d', $default_entry->fecha),
      '#required' => TRUE,
    ];
    $form['hora'] = [
      '#type' => 'datetime',
      '#date_date_element' => 'none',
      '#date_time_element' => 'time',
      '#date_time_format' => 'H:i',
      '#default_value' => new DrupalDateTime(date('H:i', $default_entry->hora)),
      '#title' => $this->t('Hour'),
      '#required' => TRUE,
    ];
    $form['descripcion'] = [
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#default_value' => $default_entry->descripcion,
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Update'),
    ];
    $form['add']['return'] = [
      '#type' => 'link',
      '#title' => t('Return to list'),
      '#url' => Url::fromRoute('citas'),
    ];

    return $form;
  }

  /**
   * AJAX callback handler for the pid select.
   *
   * When the id changes, populates the defaults from the database in the form.
   */
  public function updateCallback(array $form, FormStateInterface $form_state) {
    // Gather the DB results from $form_state.
    $entries = $form_state->getValue('entries');
    // Use the specific entry for this $form_state.
    $entry = $entries[$form_state->getValue('id')];
    // Setting the #value of items is the only way I was able to figure out
    // to get replaced defaults on these items. #default_value will not do it
    // and shouldn't.
    foreach (['nombre', 'documento', 'email', 'fecha', 'hora', 'descripcion'] as $item) {
      if ($item == 'fecha') {
        $form[$item]['#value'] = date('Y-m-d', $entry->$item);
      } elseif($item == 'hora') {
        $form[$item]['#value'] = date('H:i', $entry->$item);
      } else {
        $form[$item]['#value'] = $entry->$item;
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // TODO: Validate form
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the submitted appointment.
    $entry = [
      'id' => $form_state->getValue('id'),
      'nombre' => $form_state->getValue('nombre'),
      'documento' => $form_state->getValue('documento'),
      'email' => $form_state->getValue('email'),
      'fecha' => strtotime($form_state->getValue('fecha')),
      'hora' => strtotime($form_state->getValue('hora')),
      'descripcion' => $form_state->getValue('descripcion'),
    ];
    $count = CitasStorage::update($entry);

    drupal_set_message($this->t('Updated appointment'));
  }

}
