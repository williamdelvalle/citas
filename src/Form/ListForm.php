<?php

/**
 * @file
 * ListForm
 */

namespace Drupal\citas\Form;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\citas\CitasStorage;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Form to add a database entry, with all the interesting fields.
 */
class ListForm implements FormInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    // Mensaje descriptivo
    $form['message'] = [
      '#markup' => $this->t('List of appointments in the database.'),
    ];

    // Enlace para crear citas
    $form['add'] = [
      '#type' => 'link',
      '#title' => t('Add an appointment'),
      '#url' => Url::fromRoute('citas_add'),
      '#prefix' => '<br /><br />',
    ];

    $rows = [];

    // Celdas en la cabecera
    $headers = [t('Id'), t('Name'), t('Document'), t('Email'), t('Date'),
    t('Hour'), t('Description'), t('Edit'), t('Delete')];

    // Obtenemos la lista de citas del estado del form
    $citas = $form_state->get('citas');
    if ($citas === NULL) {
      $citas = $this->getCitas();
      $form_state->set('citas', $citas);
    }

    // Creamos la tabla con checkboxes
    $form['table'] = [
      '#type' => 'tableselect',
      '#title' => $this->t('Appointments'),
      '#header' => $headers,
      '#options' => $citas,
      '#empty' => t('No appointments found'),
      '#prefix' => '<div id="table-wrapper">',
      '#suffix' => '</div>',
    ];

    // Lista de acciones que se pueden realizar a las
    // citas seleccionadas
    $form['accion'] = [
      '#type' => 'select',
      '#title' => t('Action'),
      '#options' => [
        0 => t('None'),
        1 => t('Delete selected appointments'),
      ],
    ];

    // Impedimos que se guarde en caché
    $form_state->setCached(FALSE);

    // Acciones del form
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply actions'),
      // TODO: Fix the confirm popup
      /*'#attached' => [
        'library' => ['citas/custom']
      ],
      '#attributes' => [
        'class' => ['confirm'],
      ],*/
      '#submit' => ['::deleteCallback'],
      '#ajax' => [
        'callback' => '::refreshTableCallback',
        'wrapper' => 'table-wrapper',
      ],
    ];

    return $form;
  }

  /**
   * Método callback para obtener los ids seleccionados
   *   y los pasamos al Storage para borrarlos
   */
  public function deleteCallback(array &$form, FormStateInterface $form_state) {
    $entries = array_filter($form_state->getValue('table'));
    $accion = $form_state->getValue('accion');

    if ($accion) {
      CitasStorage::deleteByIds($entries);
    }

    $citas = $this->getCitas();
    $form_state->set('citas', $citas);

    $form_state->setRebuild();
  }

  /**
   * Método callback para refrescar la tabla
   */
  public function refreshTableCallback(array &$form, FormStateInterface $form_state) {
    return $form['table'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // TODO: Validate form
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get the submitted entries.
    $entries = array_filter($form_state->getValue('table'));
    $accion = $form_state->getValue('accion');

    if ($accion) {
      //CitasStorage::deleteByIds($entries);
    }

    return new Url('citas');
  }

  /**
  * Devolvemos la lista de citas
  */
  public function getCitas() {
    $citas = [];

    foreach ($entries = CitasStorage::load() as $entry) {
      $citas[$entry->id] = [
        [
          'id' => $entry->id,
          'nombre' => $entry->nombre,
          'documento' => $entry->documento,
          'email' => $entry->email,
          'fecha' => date('Y-m-d', $entry->fecha),
          'hora' => date('H:i', $entry->hora),
          'descripcion' => $entry->descripcion,
          'editar' => Link::fromTextAndUrl(t('Edit'),
            Url::fromRoute('citas_update', ['id' => $entry->id])),
          'borrar' => Link::fromTextAndUrl(t('Delete'),
            Url::fromRoute('citas_delete', ['id' => $entry->id])),
        ]
      ];
    }

    return $citas;
  }

}
