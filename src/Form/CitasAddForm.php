<?php

/**
 * @file
 * CitasAddForm
 */

namespace Drupal\citas\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\citas\CitasStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Form to add a database entry, with all the interesting fields.
 */
class CitasAddForm implements FormInterface, ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * We'll need this service in order to check if the user is logged in.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   *
   * We'll use the ContainerInjectionInterface pattern here to inject the
   * current user and also get the string_translation service.
   */
  public static function create(ContainerInterface $container) {
    $form = new static(
      $container->get('current_user')
    );
    // The StringTranslationTrait trait manages the string translation service
    // for us. We can inject the service here.
    $form->setStringTranslation($container->get('string_translation'));
    return $form;
  }

  /**
   * Construct the new form object.
   */
  public function __construct(AccountProxyInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'citas_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['message'] = [
      '#markup' => $this->t('Add an appointment in the database.'),
    ];

    $form['add'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Add an appointment'),
    ];
    $form['add']['nombre'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#size' => 30,
      '#required' => TRUE,
    ];
    $form['add']['documento'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Document'),
      '#size' => 15,
      '#required' => TRUE,
    ];
    $form['add']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#size' => 30,
      '#required' => TRUE,
    ];
    $form['add']['fecha'] = [
      '#type' => 'date',
      '#title' => $this->t('Date'),
      '#required' => TRUE,
    ];
    $form['add']['hora'] = [
      '#type' => 'datetime',
      '#date_date_element' => 'none',
      '#date_time_element' => 'time',
      '#date_time_format' => 'H:i',
      '#default_value' => new DrupalDateTime(),
      '#title' => $this->t('Hour'),
      '#required' => TRUE,
    ];
    $form['add']['descripcion'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#required' => TRUE,
    ];
    $form['add']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
    ];
    $form['add']['return'] = [
      '#type' => 'link',
      '#title' => t('Return to list'),
      '#url' => Url::fromRoute('citas'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Verify that the user is logged-in.
    if ($this->currentUser->isAnonymous()) {
      $form_state->setError($form['add'],
        $this->t('Debe iniciar sesión para agregar citas.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the submitted entry.
    $entry = [
      'nombre' => $form_state->getValue('nombre'),
      'documento' => $form_state->getValue('documento'),
      'email' => $form_state->getValue('email'),
      'fecha' => strtotime($form_state->getValue('fecha')),
      'hora' => strtotime($form_state->getValue('hora')),
      'descripcion' => $form_state->getValue('descripcion'),
    ];
    $return = CitasStorage::insert($entry);
    if ($return) {
      drupal_set_message($this->t('Created appointment'));
    }

    return new Url('citas');
  }

}
