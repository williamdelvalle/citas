<?php

/**
 * @file
 * CitasController
 */

namespace Drupal\citas\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\citas\CitasStorage;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Controller for Citas
 */
class CitasController extends ControllerBase {

  /**
   * Render a list of entries in the database.
   */
  public function lista() {
    $content = [];

    $content['message'] = [
      '#markup' => $this->t('Generate a list of all entries in the database.'),
    ];

    $rows = [];
    $headers = [t('Id'), t('Name'), t('Document'), t('Email'), t('Date'),
    t('Hour'), t('Description'), t('Actions'), t('Edit'), t('Delete')];
    $links = [];

    foreach ($entries = CitasStorage::load() as $entry) {
      $rows[] = [
        'id' => $entry->id,
        'nombre' => $entry->nombre,
        'documento' => $entry->documento,
        'email' => $entry->email,
        'fecha' => date('Y-m-d', $entry->fecha),
        'hora' => date('H:i', $entry->hora),
        'descripcion' => $entry->descripcion,
        'actions' => '',
        'editar' => Link::fromTextAndUrl(t('Edit'),
          Url::fromRoute('citas_update', ['id' => $entry->id])),
        'borrar' => Link::fromTextAndUrl(t('Delete'),
          Url::fromRoute('citas_delete', ['id' => $entry->id])),
      ];
    }
    
    $content['table'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => t('No entries available.'),
    ];
    // Don't cache this page.
    $content['#cache']['max-age'] = 0;

    return $content;
  }

}
