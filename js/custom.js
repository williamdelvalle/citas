(function ($) {
  Drupal.behaviors.confirm = {
    attach: function(context, settings) {
      var events =  jQuery('.confirm').clone(true).data('events');// Get the jQuery events.
      jQuery('.confirm').unbind('mousedown'); // Remove the click events.
      jQuery('.confirm').mousedown(function () {
    if (confirm('Realmente desea borrar esta(s) cita(s)?')) {
      jQuery.each(events.mousedown, function() {
        //this.handler(); // Invoke the mousedown handlers that was removed.
      });
    }
    // Prevent default action.
    return false;
      });
    }
  }
})(jQuery);

/*jQuery(document).ready(function() {
  jQuery('#confirm_delete').click(function() {
    if(!confirm('Realmente desea borrar esta(s) cita(s)?')) {
      return false;
    }
  });
});*/

